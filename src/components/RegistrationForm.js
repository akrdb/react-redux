import React, { Component } from "react";

class RegistrationForm extends Component {
    constructor(props) {
        super(props)
        this.state = {email: ''}
    }

    handleChangeEmail = (event) => {
        console.log('email changed', this)

        this.setState( prev => ({ ...prev, [event.target.name]: event.target.value } ))
    }

    handleSubmit = (event) => {
        console.log(`form submitted. Email is ${this.state.email}`)

        event.preventDefault()
    }

    render() {

        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <input type="text"
                            placeholder="E-mail"
                            name="email"
                            value={this.state.email}
                            onChange={this.handleChangeEmail}
                            />
                        <button>Save</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default RegistrationForm;