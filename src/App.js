import Dropdown from './components/Dropdown';
import Header from './components/Header';
import RegistrationForm from './components/RegistrationForm';

const menu = [
  {
    link: '/articles',
    label:'Articles'
  },
  {
    link: '/contacts',
    label:'Contacts'
  },
  {
    link: '/posts',
    label: 'Posts'
  }
]

function App() {
  return (
    <div className="container text-center">
      <div className="row">
        <div className="col">
          <RegistrationForm />
        </div>
        <div className="col">
          <Header items={menu} />
          <Dropdown />
        </div>
      </div>
      <div className='row'>
        <div className="col">
          Column
        </div>
        <div className="col">
          Column
        </div>
      </div>
    </div>
  );
}

export default App;
